<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Regression Collection</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>Global</profileName>
            <runConfigurationData>
               <entry>
                  <key>deviceName</key>
                  <value>asus P028 (Android 7.0)</value>
               </entry>
               <entry>
                  <key>deviceId</key>
                  <value>H9NPCV005069WXA</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Android</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Regression</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>Global</profileName>
            <runConfigurationData>
               <entry>
                  <key>deviceName</key>
                  <value>asus P028 (Android 7.0)</value>
               </entry>
               <entry>
                  <key>deviceId</key>
                  <value>H9NPCV005069WXA</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Android</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Regression Purchase and Refunds</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>Global</profileName>
            <runConfigurationData>
               <entry>
                  <key>deviceName</key>
                  <value>asus P028 (Android 7.0)</value>
               </entry>
               <entry>
                  <key>deviceId</key>
                  <value>H9NPCV005069WXA</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Android</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Regression 3</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
