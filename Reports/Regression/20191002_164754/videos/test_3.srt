1
00:00:00,344 --> 00:00:00,345
1. startApplication(latestBuild, false)

2
00:00:45,046 --> 00:00:45,046
5. tap(findTestObject("Log-in Page/android.widget.EditText0 - Email Address"), 0)

3
00:00:47,943 --> 00:00:47,944
9. setText(findTestObject("Log-in Page/android.widget.EditText0 - Email Address (1)"), "rabalos@events.com", 0)

4
00:00:50,284 --> 00:00:50,286
13. tap(findTestObject("Log-in Page/android.widget.EditText1 - Password (1)"), 0)

5
00:00:51,089 --> 00:00:51,090
17. setText(findTestObject("Log-in Page/android.widget.EditText1 - Password (1)"), "start123", 0)

6
00:00:53,186 --> 00:00:53,187
21. tap(findTestObject("Log-in Page/android.widget.TextView1 -  LOG IN"), 0)

7
00:00:53,927 --> 00:00:53,928
25. tap(findTestObject("android.widget.TextView3 -  Automation Event"), 0)

8
00:00:58,516 --> 00:00:58,517
29. tap(findTestObject("Set up Flow/android.widget.TextView4 -  START ON-SITE SALES"), 0)

9
00:01:00,565 --> 00:01:00,566
33. tap(findTestObject("Set up Flow/android.view.ViewGroup8 BOX OFFICE MODE"), 0)

10
00:01:01,521 --> 00:01:01,522
37. tap(findTestObject("Set up Flow/android.widget.CheckBox1 - CASH"), 0)

11
00:01:02,612 --> 00:01:02,613
41. tap(findTestObject("Set up Flow/android.widget.CheckBox2 - CHECK"), 0)

12
00:01:03,361 --> 00:01:03,361
45. tap(findTestObject("Set up Flow/android.view.ViewGroup6 - CONTINUE"), 0)

13
00:01:04,040 --> 00:01:04,041
49. tap(findTestObject("Set up Flow/android.widget.EditText0 - User First Name"), 0)

14
00:01:05,903 --> 00:01:05,905
53. setText(findTestObject("Set up Flow/android.widget.EditText0 - User First Name"), "Auto", 0)

15
00:01:08,291 --> 00:01:08,292
57. tap(findTestObject("Set up Flow/android.widget.EditText1 - User Last Name"), 0)

16
00:01:09,140 --> 00:01:09,140
61. setText(findTestObject("Set up Flow/android.widget.EditText1 - User Last Name"), "Mation", 0)

17
00:01:11,241 --> 00:01:11,242
65. tap(findTestObject("android.widget.TextView4 -  GET STARTED"), 0)

18
00:01:12,125 --> 00:01:12,126
69. tap(findTestObject("Set up Flow/android.widget.TextView4 -  SELL TICKETS"), 0)

19
00:01:13,095 --> 00:01:13,095
73. tap(findTestObject("Purchase Flow/android.view.ViewGroup28"), 0)

20
00:01:14,906 --> 00:01:14,907
77. tap(findTestObject("Purchase Flow/android.widget.TextView6 -  PURCHASE"), 0)

21
00:01:16,044 --> 00:01:16,044
81. verifyElementExist(findTestObject("Purchase Flow/android.widget.Spinner0 (1)"), 0, OPTIONAL)

22
00:01:23,684 --> 00:01:23,685
85. tap(findTestObject("Purchase Flow/android.widget.Spinner0 (1)"), 0, OPTIONAL)

23
00:01:24,954 --> 00:01:24,956
89. tap(findTestObject("Purchase Flow/android.widget.CheckedTextView2 - medium"), 0, OPTIONAL)

24
00:01:27,390 --> 00:01:27,391
93. tap(findTestObject("Purchase Flow/android.view.ViewGroup12 (1) - NEXT"), 0, CONTINUE_ON_FAILURE)

25
00:01:27,742 --> 00:01:27,743
97. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

26
00:01:30,562 --> 00:01:30,562
101. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

27
00:01:35,768 --> 00:01:35,768
105. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

28
00:01:39,266 --> 00:01:39,267
109. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCardWrong, CONTINUE_ON_FAILURE)

29
00:01:40,398 --> 00:01:40,398
113. tap(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), 0)

30
00:01:41,455 --> 00:01:41,456
117. tap(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), 0)

31
00:01:42,578 --> 00:01:42,579
121. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), month, 0)

32
00:01:44,385 --> 00:01:44,386
125. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

33
00:01:45,491 --> 00:01:45,491
129. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), year, 0)

34
00:01:47,669 --> 00:01:47,670
133. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

35
00:01:48,777 --> 00:01:48,778
137. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

36
00:01:50,941 --> 00:01:50,941
141. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

37
00:01:52,000 --> 00:01:52,001
145. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

38
00:01:54,140 --> 00:01:54,141
149. hideKeyboard()

39
00:01:55,605 --> 00:01:55,606
153. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

40
00:01:56,694 --> 00:01:56,695
157. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

41
00:01:59,548 --> 00:01:59,548
161. tap(findTestObject("Credit Card Errors/android.view.ViewGroup5 CREDIT CARD"), 0)

42
00:01:59,731 --> 00:01:59,732
165. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

43
00:02:03,719 --> 00:02:03,720
169. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

44
00:02:08,802 --> 00:02:08,802
173. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCard)

45
00:02:09,872 --> 00:02:09,873
177. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), monthWrong, 0)

46
00:02:11,961 --> 00:02:11,962
181. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

47
00:02:12,997 --> 00:02:12,998
185. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), year, 0)

48
00:02:15,142 --> 00:02:15,142
189. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

49
00:02:16,219 --> 00:02:16,219
193. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

50
00:02:18,517 --> 00:02:18,519
197. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

51
00:02:19,599 --> 00:02:19,599
201. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

52
00:02:21,853 --> 00:02:21,854
205. hideKeyboard()

53
00:02:23,247 --> 00:02:23,247
209. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

54
00:02:24,359 --> 00:02:24,360
213. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

55
00:02:26,065 --> 00:02:26,065
217. tap(findTestObject("Credit Card Errors/android.view.ViewGroup5 CREDIT CARD"), 0)

56
00:02:26,262 --> 00:02:26,263
221. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

57
00:02:30,215 --> 00:02:30,215
225. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

58
00:02:35,252 --> 00:02:35,252
229. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCard)

59
00:02:36,344 --> 00:02:36,345
233. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), month, 0)

60
00:02:38,479 --> 00:02:38,479
237. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

61
00:02:39,565 --> 00:02:39,566
241. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), yearWrong, 0)

62
00:02:41,716 --> 00:02:41,716
245. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

63
00:02:42,847 --> 00:02:42,849
249. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

64
00:02:45,003 --> 00:02:45,003
253. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

65
00:02:46,057 --> 00:02:46,058
257. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

66
00:02:48,189 --> 00:02:48,190
261. hideKeyboard()

67
00:02:49,634 --> 00:02:49,635
265. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

68
00:02:50,895 --> 00:02:50,895
269. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

