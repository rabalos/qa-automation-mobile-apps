1
00:00:00,487 --> 00:00:00,493
1. startApplication(latestBuild, false)

2
00:00:56,039 --> 00:00:56,040
5. if (verifyElementExist(findTestObject("Log-in Page/android.widget.TextView1 -  LOG IN"), 3, OPTIONAL) == true)

3
00:01:00,630 --> 00:01:00,630
2. callTestCase(findTestCase("Start Event Flow"), [:], STOP_ON_FAILURE)

4
00:01:01,016 --> 00:01:01,017
1. startApplication(latestBuild, false)

5
00:01:46,844 --> 00:01:46,845
5. tap(findTestObject("android.widget.TextView3 -  Automation Event"), 0)

6
00:01:51,383 --> 00:01:51,384
9. tap(findTestObject("Set up Flow/android.widget.TextView4 -  START ON-SITE SALES"), 0)

7
00:01:53,714 --> 00:01:53,715
13. callTestCase(findTestCase("Event Settings"), [:], CONTINUE_ON_FAILURE)

8
00:01:54,131 --> 00:01:54,131
1. tap(findTestObject("Set up Flow/android.view.ViewGroup10 - Box Office Mode"), 0)

9
00:01:55,630 --> 00:01:55,631
5. if (verifyElementNotChecked(findTestObject("Set up Flow/android.widget.CheckBox1 - CASH (1)"), 0, OPTIONAL))

10
00:01:58,045 --> 00:01:58,045
2. verifyElementChecked(findTestObject("Set up Flow/android.widget.CheckBox1 - CASH (1)"), 0, OPTIONAL)

11
00:01:58,292 --> 00:01:58,293
9. if (verifyElementNotChecked(findTestObject("Set up Flow/android.widget.CheckBox2 - CHECK (1)"), 0, OPTIONAL))

12
00:01:59,830 --> 00:01:59,831
2. checkElement(findTestObject("Set up Flow/android.widget.CheckBox2 - CHECK (1)"), 0, OPTIONAL)

13
00:02:00,024 --> 00:02:00,026
13. if (verifyElementExist(findTestObject("Set up Flow/android.view.ViewGroup22 - SUBMIT"), 0, OPTIONAL) == true)

14
00:02:00,159 --> 00:02:00,160
1. tap(findTestObject("Set up Flow/android.view.ViewGroup22 - SUBMIT"), 0, OPTIONAL)

15
00:02:01,442 --> 00:02:01,443
17. tap(findTestObject("Set up Flow/android.widget.EditText0 - User First Name"), 0)

16
00:02:04,953 --> 00:02:04,954
21. setText(findTestObject("Set up Flow/android.widget.EditText0 - User First Name"), userFirstName, 0)

17
00:02:07,592 --> 00:02:07,593
25. tap(findTestObject("Set up Flow/android.widget.EditText1 - User Last Name"), 0)

18
00:02:08,414 --> 00:02:08,415
29. setText(findTestObject("Set up Flow/android.widget.EditText1 - User Last Name"), userLastName, 0)

19
00:02:10,483 --> 00:02:10,484
33. hideKeyboard()

20
00:02:11,900 --> 00:02:11,901
37. tap(findTestObject("android.widget.TextView4 -  GET STARTED"), 0)

21
00:02:12,718 --> 00:02:12,720
9. tap(findTestObject("Set up Flow/android.widget.TextView4 -  SELL TICKETS"), 0)

22
00:02:13,654 --> 00:02:13,655
13. tap(findTestObject("Purchase Flow/android.view.ViewGroup28"), 0)

23
00:02:15,763 --> 00:02:15,764
17. tap(findTestObject("Purchase Flow/android.widget.TextView6 -  PURCHASE"), 0)

24
00:02:16,882 --> 00:02:16,882
21. verifyElementExist(findTestObject("Purchase Flow/android.widget.Spinner0 (1)"), 0, OPTIONAL)

25
00:02:23,706 --> 00:02:23,707
25. tap(findTestObject("Purchase Flow/android.widget.Spinner0 (1)"), 0, OPTIONAL)

26
00:02:25,032 --> 00:02:25,034
29. tap(findTestObject("Purchase Flow/android.widget.CheckedTextView2 - medium"), 0, OPTIONAL)

27
00:02:27,785 --> 00:02:27,788
33. scrollToText("APPLY", STOP_ON_FAILURE)

28
00:02:31,532 --> 00:02:31,534
37. tap(findTestObject("Purchase Flow/android.widget.Spinner1 - SELECT OTHER"), 0)

29
00:02:31,762 --> 00:02:31,762
41. tap(findTestObject("Purchase Flow/android.widget.CheckedTextView2 - Medium (1)"), 0)

30
00:02:33,895 --> 00:02:33,896
45. tap(findTestObject("Purchase Flow/android.view.ViewGroup40 - PLUS 2"), 0)

31
00:02:34,229 --> 00:02:34,230
49. tap(findTestObject("Purchase Flow/android.view.ViewGroup40 - ADD to CART 2"), 2, STOP_ON_FAILURE)

32
00:02:35,363 --> 00:02:35,363
53. tap(findTestObject("Purchase Flow/android.view.ViewGroup12 (1) - NEXT"), 0, CONTINUE_ON_FAILURE)

33
00:02:37,147 --> 00:02:37,148
57. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

34
00:02:40,554 --> 00:02:40,554
61. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

35
00:02:45,942 --> 00:02:45,943
65. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

36
00:02:49,398 --> 00:02:49,400
69. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCardWrong, CONTINUE_ON_FAILURE)

37
00:02:50,582 --> 00:02:50,583
73. tap(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), 0)

38
00:02:51,752 --> 00:02:51,753
77. tap(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), 0)

39
00:02:53,115 --> 00:02:53,116
81. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), month, 0)

40
00:02:54,847 --> 00:02:54,850
85. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

41
00:02:55,951 --> 00:02:55,951
89. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), year, 0)

42
00:02:58,117 --> 00:02:58,118
93. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

43
00:02:59,329 --> 00:02:59,330
97. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

44
00:03:01,529 --> 00:03:01,529
101. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

45
00:03:02,728 --> 00:03:02,729
105. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

46
00:03:04,975 --> 00:03:04,977
109. hideKeyboard()

47
00:03:06,280 --> 00:03:06,281
113. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

48
00:03:07,581 --> 00:03:07,582
117. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

49
00:03:11,590 --> 00:03:11,591
121. tap(findTestObject("Credit Card Errors/android.view.ViewGroup5 CREDIT CARD"), 0)

50
00:03:11,757 --> 00:03:11,758
125. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

51
00:03:16,094 --> 00:03:16,095
129. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

52
00:03:21,554 --> 00:03:21,554
133. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCard)

53
00:03:22,701 --> 00:03:22,702
137. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), monthWrong, 0)

54
00:03:25,049 --> 00:03:25,050
141. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

55
00:03:26,135 --> 00:03:26,136
145. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), year, 0)

56
00:03:28,341 --> 00:03:28,342
149. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

57
00:03:29,561 --> 00:03:29,562
153. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

58
00:03:31,757 --> 00:03:31,758
157. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

59
00:03:32,925 --> 00:03:32,925
161. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

60
00:03:35,178 --> 00:03:35,179
165. hideKeyboard()

61
00:03:36,629 --> 00:03:36,629
169. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

62
00:03:37,862 --> 00:03:37,862
173. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

63
00:03:39,712 --> 00:03:39,712
177. tap(findTestObject("Credit Card Errors/android.view.ViewGroup5 CREDIT CARD"), 0)

64
00:03:39,879 --> 00:03:39,880
181. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

65
00:03:44,210 --> 00:03:44,211
185. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

66
00:03:49,369 --> 00:03:49,370
189. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCard)

67
00:03:50,422 --> 00:03:50,422
193. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), month, 0)

68
00:03:52,645 --> 00:03:52,647
197. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

69
00:03:53,769 --> 00:03:53,770
201. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), yearWrong, 0)

70
00:03:55,960 --> 00:03:55,961
205. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

71
00:03:57,059 --> 00:03:57,060
209. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

72
00:03:59,253 --> 00:03:59,254
213. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

