1
00:00:00,340 --> 00:00:00,340
1. startApplication(latestBuild, false)

2
00:00:44,002 --> 00:00:44,003
5. tap(findTestObject("Log-in Page/android.widget.EditText0 - Email Address"), 0)

3
00:00:46,705 --> 00:00:46,706
9. setText(findTestObject("Log-in Page/android.widget.EditText0 - Email Address (1)"), "rabalos@events.com", 0)

4
00:00:48,998 --> 00:00:48,998
13. tap(findTestObject("Log-in Page/android.widget.EditText1 - Password (1)"), 0)

5
00:00:49,774 --> 00:00:49,774
17. setText(findTestObject("Log-in Page/android.widget.EditText1 - Password (1)"), "start123", 0)

6
00:00:51,870 --> 00:00:51,871
21. tap(findTestObject("Log-in Page/android.widget.TextView1 -  LOG IN"), 0)

7
00:00:52,607 --> 00:00:52,608
25. tap(findTestObject("android.widget.TextView3 -  Automation Event"), 0)

8
00:00:57,001 --> 00:00:57,001
29. tap(findTestObject("Set up Flow/android.widget.TextView4 -  START ON-SITE SALES"), 0)

9
00:00:59,072 --> 00:00:59,073
33. tap(findTestObject("Set up Flow/android.view.ViewGroup8 BOX OFFICE MODE"), 0)

10
00:01:00,029 --> 00:01:00,029
37. tap(findTestObject("Set up Flow/android.widget.CheckBox1 - CASH"), 0)

11
00:01:01,433 --> 00:01:01,433
41. tap(findTestObject("Set up Flow/android.widget.CheckBox2 - CHECK"), 0)

12
00:01:02,250 --> 00:01:02,251
45. tap(findTestObject("Set up Flow/android.view.ViewGroup6 - CONTINUE"), 0)

13
00:01:02,897 --> 00:01:02,898
49. tap(findTestObject("Set up Flow/android.widget.EditText0 - User First Name"), 0)

14
00:01:04,812 --> 00:01:04,813
53. setText(findTestObject("Set up Flow/android.widget.EditText0 - User First Name"), "Auto", 0)

15
00:01:07,168 --> 00:01:07,169
57. tap(findTestObject("Set up Flow/android.widget.EditText1 - User Last Name"), 0)

16
00:01:07,981 --> 00:01:07,982
61. setText(findTestObject("Set up Flow/android.widget.EditText1 - User Last Name"), "Mation", 0)

17
00:01:10,081 --> 00:01:10,082
65. tap(findTestObject("android.widget.TextView4 -  GET STARTED"), 0)

18
00:01:10,899 --> 00:01:10,899
69. tap(findTestObject("Set up Flow/android.widget.TextView4 -  SELL TICKETS"), 0)

19
00:01:11,862 --> 00:01:11,863
73. tap(findTestObject("Purchase Flow/android.view.ViewGroup28"), 0)

20
00:01:13,697 --> 00:01:13,698
77. tap(findTestObject("Purchase Flow/android.widget.TextView6 -  PURCHASE"), 0)

21
00:01:14,829 --> 00:01:14,830
81. delay(2)

22
00:01:16,856 --> 00:01:16,857
85. verifyElementExist(findTestObject("Purchase Flow/android.widget.Spinner0 (1)"), 0, OPTIONAL)

23
00:01:22,119 --> 00:01:22,120
89. tap(findTestObject("Purchase Flow/android.widget.Spinner0 (1)"), 0, OPTIONAL)

24
00:01:23,321 --> 00:01:23,323
93. tap(findTestObject("Purchase Flow/android.widget.CheckedTextView2 - medium"), 0, OPTIONAL)

25
00:01:25,848 --> 00:01:25,848
97. tap(findTestObject("Purchase Flow/android.view.ViewGroup12 (1) - NEXT"), 0, CONTINUE_ON_FAILURE)

26
00:01:26,146 --> 00:01:26,147
101. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

27
00:01:28,871 --> 00:01:28,871
105. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

28
00:01:33,932 --> 00:01:33,933
109. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

29
00:01:37,322 --> 00:01:37,322
113. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCardWrong, CONTINUE_ON_FAILURE)

30
00:01:38,331 --> 00:01:38,332
117. tap(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), 0)

31
00:01:39,461 --> 00:01:39,461
121. tap(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), 0)

32
00:01:40,555 --> 00:01:40,555
125. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), month, 0)

33
00:01:42,328 --> 00:01:42,328
129. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

34
00:01:43,385 --> 00:01:43,385
133. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), year, 0)

35
00:01:45,546 --> 00:01:45,546
137. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

36
00:01:46,601 --> 00:01:46,601
141. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

37
00:01:48,746 --> 00:01:48,747
145. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

38
00:01:49,796 --> 00:01:49,797
149. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

39
00:01:52,005 --> 00:01:52,005
153. hideKeyboard()

40
00:01:53,412 --> 00:01:53,412
157. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

41
00:01:54,610 --> 00:01:54,611
161. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

42
00:01:57,178 --> 00:01:57,178
165. tap(findTestObject("Credit Card Errors/android.view.ViewGroup5 CREDIT CARD"), 0)

43
00:01:57,310 --> 00:01:57,311
169. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

44
00:02:00,355 --> 00:02:00,355
173. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

45
00:02:05,446 --> 00:02:05,447
177. sendKeys(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), creditCard)

46
00:02:06,418 --> 00:02:06,419
181. setText(findTestObject("Purchase Flow/android.widget.EditText1 - Month"), monthWrong, 0)

47
00:02:08,615 --> 00:02:08,616
185. tap(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), 0)

48
00:02:09,684 --> 00:02:09,685
189. setText(findTestObject("Purchase Flow/android.widget.EditText2 - Year"), year, 0)

49
00:02:11,846 --> 00:02:11,846
193. tap(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), 0)

50
00:02:12,933 --> 00:02:12,934
197. setText(findTestObject("Purchase Flow/android.widget.EditText3 - CVV Number"), cvv, 0)

51
00:02:15,070 --> 00:02:15,072
201. tap(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), 0)

52
00:02:16,161 --> 00:02:16,162
205. setText(findTestObject("Purchase Flow/android.widget.EditText4 - Zip Code"), zip, 0)

53
00:02:18,287 --> 00:02:18,288
209. hideKeyboard()

54
00:02:19,616 --> 00:02:19,617
213. tap(findTestObject("Purchase Flow/android.widget.TextView8 -  PURCHASE"), 0, STOP_ON_FAILURE)

55
00:02:20,931 --> 00:02:20,932
217. verifyElementExist(findTestObject("Credit Card Errors/android.widget.TextView0 - Payment Failed"), 0)

56
00:02:22,608 --> 00:02:22,608
221. tap(findTestObject("Credit Card Errors/android.view.ViewGroup5 CREDIT CARD"), 0)

57
00:02:22,743 --> 00:02:22,743
225. tap(findTestObject("Purchase Flow/android.widget.TextView9 - Enter card information manually"), 0)

58
00:02:26,146 --> 00:02:26,146
229. tap(findTestObject("Purchase Flow/android.widget.EditText0 - Card Number"), 0)

