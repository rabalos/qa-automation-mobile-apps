import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

if (Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 3, FailureHandling.OPTIONAL) == 
true) {
    WebUI.callTestCase(findTestCase('EverySale-Test Cases/Log in Box Office'), [:], FailureHandling.OPTIONAL)
} else {
    WebUI.callTestCase(findTestCase('EverySale-Test Cases/Start Event Flow'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  SELL TICKETS'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup28'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView6 -  PURCHASE'), 0)

Mobile.verifyElementExist(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - medium'), 0, FailureHandling.OPTIONAL)

Mobile.scrollToText('APPLY', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner1 - SELECT OTHER'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - Medium (1)'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup40 - PLUS 2'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup40 - ADD to CART 2'), 2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup12 (1) - NEXT'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCardWrong, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), GlobalVariable.month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), GlobalVariable.year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), GlobalVariable.cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), GlobalVariable.zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), GlobalVariable.creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), monthWrong, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), GlobalVariable.year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), GlobalVariable.cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), GlobalVariable.zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), yearWrong, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), cvvWrong, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), zipWrong, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView2 -  IM FINISHED'), 0)

