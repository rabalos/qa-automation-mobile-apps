import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false, FailureHandling.CONTINUE_ON_FAILURE)

if (Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 3, FailureHandling.OPTIONAL) == 
true) {
    WebUI.callTestCase(findTestCase('EverySale-Test Cases/Log in Box Office'), [:], FailureHandling.OPTIONAL)
} else {
    WebUI.callTestCase(findTestCase('EverySale-Test Cases/Start Event Flow'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup5 - MANAGE'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup8 - MANAGE TICKETS'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup16 - ATTENDEE DETAILS'), 0)

Mobile.tap(findTestObject('android.view.ViewGroup13'), 0)

Mobile.setText(findTestObject('android.widget.EditText0'), password, 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup7 - LOG IN'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox0 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox1 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox2 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox3 - REFUND TAXES'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.EditText0 (1)'), 0)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText0 (1)'), refund1, 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup18 - Cancel Refund Button'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup6 - CANCEL ITEMS'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup16 - CANCEL OTHER STUFF'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup6 - CANCEL ITEMS'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.ImageView0 - BACK'), 0)

Mobile.pressBack()

Mobile.pressBack()

Mobile.delay(2)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup5 - MANAGE'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup8'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup17 - Second Row'), 0)

Mobile.tap(findTestObject('android.view.ViewGroup13'), 0)

Mobile.setText(findTestObject('android.widget.EditText0'), password, 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup7 - LOG IN'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox0 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox1 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox2 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.EditText0 (1)'), 0)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText0 (1)'), refund100, 0)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText2- FEE'), refund2, 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.CheckBox3 (1)'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup20 - OTHER STUFF REFUND 4'), 0)

Mobile.pressBack()

Mobile.pressBack()

WebUI.callTestCase(findTestCase('EverySale App - Regressions/OMA - 269 Manage Tickets'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('EverySale App - Regressions/OMA - 99 Switch Users'), [:], FailureHandling.STOP_ON_FAILURE)

