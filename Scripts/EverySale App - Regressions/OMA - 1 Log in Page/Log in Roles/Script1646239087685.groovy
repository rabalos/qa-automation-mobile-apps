import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), timer, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText1 - Password'), 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password'), 'start123', 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Log-in Page/android.view.ViewGroup12 - LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Log-in Page/android.view.ViewGroup7 - No Access Toaster'), 0)

Mobile.delay(8, FailureHandling.STOP_ON_FAILURE)

Mobile.clearText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), accountant, 0)

Mobile.tap(findTestObject('Log-in Page/android.view.ViewGroup12 - LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Log-in Page/android.view.ViewGroup7 - No Access Toaster'), 0)

Mobile.delay(8)

Mobile.clearText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), assistant, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.verifyElementExist(findTestObject('Log-in Page/android.view.ViewGroup7 - No Access Toaster'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.clearText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), coordinator, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.verifyElementExist(findTestObject('Log-in Page/android.view.ViewGroup7 - No Access Toaster'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.clearText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), managerLimited, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.tap(findTestObject('Log-in Page/android.view.ViewGroup12 - LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('EverySale App - Regressions/OMA - 3 Org Settings Page'), [:], FailureHandling.STOP_ON_FAILURE)

