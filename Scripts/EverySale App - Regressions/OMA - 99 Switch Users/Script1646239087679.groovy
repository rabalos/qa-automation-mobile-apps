import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

if (Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 0, FailureHandling.CONTINUE_ON_FAILURE) == 
true) {
    WebUI.callTestCase(findTestCase('EverySale-Test Cases/Log in Box Office'), [:], FailureHandling.CONTINUE_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('EverySale-Test Cases/Start Event Flow'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.tap(findTestObject('Hamburger Nav/android.widget.TextView0 - HAMBURGER NAV'), 0)

Mobile.tap(findTestObject('Hamburger Nav/android.view.ViewGroup21 - SWITCH USERS'), 0)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText0 - Password'), 'start123', 0)

Mobile.tap(findTestObject('Switch Users/android.view.ViewGroup8 - LOG IN BUTTON'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.EditText0 - New User First Name'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText0 - New User First Name'), 'New', 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.EditText1 - New User Last Name'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText1 - New User Last Name'), 'User 123', 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup10 - SWITCH USER BUTTON'), 0)

Mobile.verifyElementText(findTestObject('Cancel Refund/android.widget.TextView2 - User New User'), 'User: New User 123')

WebUI.callTestCase(findTestCase('EverySale-Test Cases/Purchase Flow'), [:], FailureHandling.STOP_ON_FAILURE)

