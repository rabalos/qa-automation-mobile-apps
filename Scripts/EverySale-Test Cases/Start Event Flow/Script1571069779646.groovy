import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

Mobile.tap(findTestObject('Event Home Page/android.view.ViewGroup15 - Automation Event'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  START ON-SITE SALES'), 0)

WebUI.callTestCase(findTestCase('EverySale-Test Cases/Event Settings'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Set up Flow/android.widget.EditText0 - User First Name'), 0)

Mobile.setText(findTestObject('Set up Flow/android.widget.EditText0 - User First Name'), GlobalVariable.userFirstName, 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.EditText1 - User Last Name'), 0)

Mobile.setText(findTestObject('Set up Flow/android.widget.EditText1 - User Last Name'), GlobalVariable.userLastName, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Set up Flow/android.view.ViewGroup9 - GET STARTED'), 0)

