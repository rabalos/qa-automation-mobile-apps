import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), GlobalVariable.emailManager, 
    1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), GlobalVariable.password, 
    1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('android.widget.TextView3 -  Automation Event'), 0)

Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup10 - START SALES'), 0)

Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup10 - SELF SERVICE MODE'), 0)

Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup6 - CONTINUE'), 0)

for (int i = 0; i < 15; i++) {
    Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup7 PURCHASE REGISTRATION'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup23 TICKET 1 ADD'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup12 - PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(5)

    not_run: Mobile.tap(findTestObject('Self-Service/android.widget.EditText0 - First Name (optional)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    String[] array = findTestData('Attendee_Names').getAllData()

    int random = new Random().nextInt(array.length)

    Mobile.sendKeys(findTestObject('Self-Service/android.widget.EditText0 - First Name (optional)'), findTestData('Attendee_Names').getValue(
            1, random), FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.tap(findTestObject('Self-Service/android.widget.EditText1 - Last Name (optional)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.sendKeys(findTestObject('Self-Service/android.widget.EditText1 - Last Name (optional)'), findTestData('Attendee_Names').getValue(
            2, random), FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.tap(findTestObject('Self-Service/android.widget.EditText2 - Email Address'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.sendKeys(findTestObject('Self-Service/android.widget.EditText2 - Email Address'), email, FailureHandling.STOP_ON_FAILURE)

    Mobile.hideKeyboard(FailureHandling.STOP_ON_FAILURE)

    not_run: WebUI.verifyElementText(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), '', FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(2)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView1 - small'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

    Mobile.scrollToText('APPLY', FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup30 - PLUS OTHER'), 0)

    Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup30 - ADD TO CART1'), 0)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner1 - SELECT OTHER'), 0)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - Medium (1)'), 0)

    Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup38 - ADD TO CART 2'), 0)

    Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup12 - PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0, FailureHandling.STOP_ON_FAILURE)
	
    Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), findTestData('Attendee_Names').getDataInfo(
            3, 1..16), FailureHandling.CONTINUE_ON_FAILURE)

    //Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), GlobalVariable.creditCard, FailureHandling.STOP_ON_FAILURE)
    Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), GlobalVariable.month, 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), GlobalVariable.year, 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), GlobalVariable.cvv, 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), GlobalVariable.zip, 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.hideKeyboard(FailureHandling.STOP_ON_FAILURE)

    if (true) {
        Mobile.verifyElementExist(findTestObject('Self-Service/android.view.ViewGroup8 - PRINT'), 0)

        Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup8 - PRINT'), 0)

        Mobile.tap(findTestObject('Self-Service/android.view.ViewGroup8 - FINISHED 2'), 0)
    } else {
    }
    
    Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView2 -  IM FINISHED'), 0, FailureHandling.STOP_ON_FAILURE)

    not_run: Mobile.switchToPortrait()
}

