import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (Mobile.verifyElementExist(findTestObject('android.view.ViewGroup5 - Landscape Manage'), 0)) {
    Mobile.tap(findTestObject('android.view.ViewGroup5 - Landscape Manage'), 0)
} else {
    Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup6 - MANAGE TICKETS 1'), 0)
}

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup8'), 0)

Mobile.tap(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), 0)

Mobile.sendKeys(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), 'eventstest03@gmail.com')

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup9 - SEARCH BUTTON'), 0)

Mobile.delay(2)

Mobile.verifyElementExist(findTestObject('Cancel Refund/android.widget.TextView13 - O-GYZTO-MRSGM'), 0)

Mobile.clearText(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), 0)

Mobile.sendKeys(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), 'EEEEE EEEEEEEEE')

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup9 - SEARCH BUTTON'), 0)

Mobile.verifyElementExist(findTestObject('Cancel Refund/android.widget.TextView11 - EEEEEEEEE'), 0)

Mobile.delay(1)

Mobile.setText(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), 'O-GYZTQ-MBSG4', 0)

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup9 - SEARCH BUTTON'), 0)

Mobile.delay(2)

Mobile.verifyElementText(findTestObject('Cancel Refund/android.widget.TextView14 - O-GYZTQ-MBSG4'), 'O-GYZTQ-MBSG4')

Mobile.clearText(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), 0)

Mobile.sendKeys(findTestObject('Cancel Refund/android.widget.EditText0 - SEARCH'), '0001')

Mobile.tap(findTestObject('Cancel Refund/android.view.ViewGroup9 - SEARCH BUTTON'), 0)

Mobile.verifyElementExist(findTestObject('Cancel Refund/android.widget.TextView15 - 0001'), 0)

Mobile.pressBack()

