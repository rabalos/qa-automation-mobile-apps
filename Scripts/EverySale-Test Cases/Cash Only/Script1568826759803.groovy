import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address (1)'), GlobalVariable.emailManager, 
    1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), GlobalVariable.password, 
    1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.verifyElementExist(findTestObject('android.widget.TextView2 - Welcome'), 0)

Mobile.tap(findTestObject('android.widget.TextView3 -  Automation Event'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  START ON-SITE SALES'), 0)

Mobile.tap(findTestObject('Set up Flow/android.view.ViewGroup8 BOX OFFICE MODE'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.CheckBox1 - CASH'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckBox0 - CREDITDEBIT CARD'), 0)

Mobile.tap(findTestObject('Set up Flow/android.view.ViewGroup6 - CONTINUE'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.EditText0 - User First Name'), 0)

Mobile.setText(findTestObject('Set up Flow/android.widget.EditText0 - User First Name'), GlobalVariable.userFirstName, 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.EditText1 - User Last Name'), 0)

Mobile.setText(findTestObject('Set up Flow/android.widget.EditText1 - User Last Name'), GlobalVariable.userLastName, 0)

Mobile.tap(findTestObject('android.widget.TextView4 -  GET STARTED'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  SELL TICKETS'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup23 (1)'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView6 -  PURCHASE'), 0)

Mobile.delay(2)

if (true) {
    Mobile.verifyElementExist(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView3 - large'), 0, FailureHandling.OPTIONAL)

    Mobile.scrollToText('APPLY', FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup30 - PLUS OTHER'), 0)

    Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup30 - ADD TO CART1'), 0)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner1 - SELECT OTHER'), 0)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - Medium (1)'), 0)

    Mobile.delay(2)

    Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup38 - ADD TO CART 2'), 0)
}

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup12 (1) - NEXT'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Cash Received 0.00'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText0 - Cash Received 0.00'), cash, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup21 (1)'), 0)

WebUI.delay(2)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView26 -  SUBMIT'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView2 -  IM FINISHED'), 0)

