import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  SELL TICKETS'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup28'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup28'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup33 - ADD TICKET'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText0 - Access Code'), '12345', 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup34 - GO'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView6 -  PURCHASE'), 0)

Mobile.verifyElementExist(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - medium'), 0, FailureHandling.OPTIONAL)

Mobile.scrollToText('Ticket 2')

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup31 - Ticket 2 Attendee Item'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView1 - Ticket 2 Attendee Item small'), 0, FailureHandling.OPTIONAL)

Mobile.scrollToText('Ticket 3', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup38 - Ticket 3 Attende Item'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView3 - large'), 0)

Mobile.scrollToText('APPLY', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner1 - SELECT OTHER'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - Medium (1)'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup40 - PLUS 2'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup40 - ADD to CART 2'), 2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup12 (1) - NEXT'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

String[] array = findTestData('Data Files/Credit Cards').getAllData()

int random = new Random().nextInt(array.length)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), findTestData('Credit Cards').getValue(
        1, random), FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), GlobalVariable.month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), GlobalVariable.year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), GlobalVariable.cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), GlobalVariable.zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.delay(0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView2 -  IM FINISHED'), 0)

