import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address (1)'), GlobalVariable.emailOwner, 1, 
    FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), GlobalVariable.password, 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Log out Settings/android.view.ViewGroup4'), 0)

Mobile.tap(findTestObject('Log out Settings/android.widget.TextView1 - Privacy Policy (1)'), 0)

Mobile.verifyElementExist(findTestObject('Log out Settings/android.view.View16 - Privacy Policy'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Log out Settings/android.widget.TextView3 - Help'), 0)

Mobile.verifyElementExist(findTestObject('Log out Settings/android.webkit.WebView0 - FAQ Home  Events.com'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Log out Settings/android.widget.TextView5 - Log Out'), 0)

Mobile.tap(findTestObject('Log out Settings/android.view.ViewGroup6'), 0)

Mobile.verifyElementExist(findTestObject('Log out Settings/android.widget.TextView0 - LOG IN (1)'), 0)

Mobile.closeApplication()

