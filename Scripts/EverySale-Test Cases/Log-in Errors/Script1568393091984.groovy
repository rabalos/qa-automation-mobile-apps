import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, false)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 0)

Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView2 -  Required'), 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView2 - FORGOT PASSWORD'), 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address (1)'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address (1)'), wrongEmail, 0)

Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView1 -  Not a valid email address'), 0)

Mobile.tap(findTestObject('Log-in Page/android.view.ViewGroup4  - SUBMIT'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), email, 0)

Mobile.tap(findTestObject('Log-in Page/android.view.ViewGroup4  - SUBMIT'), 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address (1)'), wrongEmail, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 0)

Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView1 -  Not a valid email address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), email, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), wrongPassword, 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 0)

Mobile.verifyElementExist(findTestObject('Log-in Page/android.widget.TextView3 - Oops we did not recognize your log-in information please try again.'), 
    0)

not_run: Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), password, 0)

not_run: Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 0)

not_run: Mobile.verifyElementExist(findTestObject('android.widget.TextView2 - Welcome'), 0)

