import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.latestBuild1, true)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText0 - Email Address'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText0 - Email Address (1)'), 'rabalos@events.com', 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), 0)

Mobile.setText(findTestObject('Log-in Page/android.widget.EditText1 - Password (1)'), 'start123', 0)

Mobile.tap(findTestObject('Log-in Page/android.widget.TextView1 -  LOG IN'), 0)

not_run: Mobile.verifyElementExist(findTestObject('android.widget.TextView2 - Welcome'), 0)

Mobile.tap(findTestObject('android.widget.TextView3 -  Automation Event'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  START ON-SITE SALES'), 0)

Mobile.tap(findTestObject('Set up Flow/android.view.ViewGroup8 BOX OFFICE MODE'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.CheckBox1 - CASH'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.CheckBox2 - CHECK'), 0)

Mobile.tap(findTestObject('Set up Flow/android.view.ViewGroup6 - CONTINUE'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.EditText0 - User First Name'), 0)

Mobile.setText(findTestObject('Set up Flow/android.widget.EditText0 - User First Name'), 'Auto', 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.EditText1 - User Last Name'), 0)

Mobile.setText(findTestObject('Set up Flow/android.widget.EditText1 - User Last Name'), 'Mation', 0)

Mobile.tap(findTestObject('android.widget.TextView4 -  GET STARTED'), 0)

Mobile.tap(findTestObject('Set up Flow/android.widget.TextView4 -  SELL TICKETS'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup28'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView6 -  PURCHASE'), 0)

Mobile.delay(2)

if (true) {
    Mobile.verifyElementExist(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.Spinner0 (1)'), 0, FailureHandling.OPTIONAL)

    Mobile.tap(findTestObject('Purchase Flow/android.widget.CheckedTextView2 - medium'), 0, FailureHandling.OPTIONAL)
}

Mobile.tap(findTestObject('Purchase Flow/android.view.ViewGroup12 (1) - NEXT'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCardWrong, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), GlobalVariable.month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), GlobalVariable.year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), GlobalVariable.cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), GlobalVariable.zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), GlobalVariable.creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), monthWrong, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), GlobalVariable.year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), GlobalVariable.cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), GlobalVariable.zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), yearWrong, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), cvvWrong, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), zip, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Credit Card Errors/android.widget.TextView0 - Payment Failed'), 0)

Mobile.tap(findTestObject('Credit Card Errors/android.view.ViewGroup5 CREDIT CARD'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView9 - Enter card information manually'), 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), 0)

Mobile.sendKeys(findTestObject('Purchase Flow/android.widget.EditText0 - Card Number'), creditCard)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText1 - Month'), month, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText2 - Year'), year, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText3 - CVV Number'), cvv, 0)

Mobile.tap(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), 0)

Mobile.setText(findTestObject('Purchase Flow/android.widget.EditText4 - Zip Code'), zipWrong, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Purchase Flow/android.widget.TextView8 -  PURCHASE'), 0, FailureHandling.STOP_ON_FAILURE)

