import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Event Settings/android.view.ViewGroup29 - BOX OFFICE MODE'), 0)

if (Mobile.verifyElementNotChecked(findTestObject('Event Settings/android.widget.CheckBox1 - CASH'), 0, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('Event Settings/android.widget.CheckBox1 - CASH'), 0, FailureHandling.OPTIONAL)
} else {
    Mobile.tap(findTestObject('Event Settings/android.widget.CheckBox1 - CASH (1)'), 0, FailureHandling.OPTIONAL)
}

if (Mobile.verifyElementNotChecked(findTestObject('Event Settings/android.widget.CheckBox2 - CHECK'), 0, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('Event Settings/android.widget.CheckBox2 - CHECK'), 0, FailureHandling.OPTIONAL)
} else {
    Mobile.checkElement(findTestObject('Event Settings/android.widget.CheckBox2 - CHECK'), 0, FailureHandling.OPTIONAL)
}

if (Mobile.verifyElementExist(findTestObject('Event Settings/android.view.ViewGroup37 - SUBMIT ASUS'), 0, FailureHandling.OPTIONAL) == 
true) {
    Mobile.tap(findTestObject('Event Settings/android.view.ViewGroup37 - SUBMIT ASUS'), 0, FailureHandling.STOP_ON_FAILURE)
} else {
    Mobile.scrollToText('SUBMIT', FailureHandling.OPTIONAL)

    Mobile.tap(findTestObject('Event Settings/android.view.ViewGroup30 - SUBMIT landscape ASUS'), 0)
}

