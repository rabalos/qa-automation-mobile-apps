import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/ro/Katalon Studio/Every_App/AppBuilds/On-Site appTest2.apk', true)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.EditText - Email Address'), 0)

Mobile.setText(findTestObject('Object Repository/New Samsung/android.widget.EditText - Email Address (2)'), 'rabalos@events.com', 
    0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.EditText - Password'), 0)

Mobile.setText(findTestObject('Object Repository/New Samsung/android.widget.EditText - Password (1)'), 'start123', 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.view.ViewGroup (2)'), 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  ZZZ_2021 Florida Renaissance Festival'), 
    0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.view.ViewGroup (3)'), 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.view.ViewGroup (4)'), 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.CheckBox'), 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  SUBMIT'), 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.EditText - User First Name'), 0)

Mobile.setText(findTestObject('Object Repository/New Samsung/android.widget.EditText - User First Name (1)'), 'test', 0)

Mobile.setText(findTestObject('Object Repository/New Samsung/android.widget.EditText - User Last Name'), 'user', 0)

Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  GET STARTED'), 0)

for (int i = 0; i < 30; i++) {
    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  SELL TICKETS'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.view.ViewGroup (5)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  PURCHASE'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.Spinner'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.CheckedTextView - Required'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  NEXT'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.view.ViewGroup (6)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  COMPLETE ORDER'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  IM FINISHED'), 0)
}

