import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/ro/Downloads/On-Site appTest2.apk', true)

WebUI.callTestCase(findTestCase('Samsung Tab - EverySale/Samsung Start Event Flow - RENFEST'), [:], FailureHandling.STOP_ON_FAILURE)

for (int i = 0; i < 15; i++) {
    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  SELL TICKETS'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.view.ViewGroup (5)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  PURCHASE'), 0)

    String[] array = findTestData('Attendee_Names').getAllData()

    int random = new Random().nextInt(array.length)

    println((array[random]).toString())

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.EditText - First Name (optional)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.sendKeys(findTestObject('New Samsung/android.widget.EditText - First Name (optional)'), findTestData('Attendee_Names').getValue(
            1, random), FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.EditText - Last Name (optional)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.sendKeys(findTestObject('New Samsung/android.widget.EditText - Last Name (optional)'), findTestData('Attendee_Names').getValue(
            2, random), FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.tap(findTestObject('New Samsung/android.widget.EditText - Email Address (optional)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.setText(findTestObject('Object Repository/New Samsung/android.widget.EditText - Email Address (optional)'), 'rabalos+01@events.com', 
        0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView - Select (2)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.CheckedTextView - Required (2)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  NEXT (2)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  COMPLETE ORDER (2)'), 0)

    Mobile.tap(findTestObject('Object Repository/New Samsung/android.widget.TextView -  IM FINISHED (1)'), 0)
}

Mobile.closeApplication()

