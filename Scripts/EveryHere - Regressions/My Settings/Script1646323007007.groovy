import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('EveryHere-Test Cases/Log in page'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(45)

Mobile.tap(findTestObject('Check-In App/My Settings Page/android.widget.TextView'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - My Settings'), 0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Account Information'), 
    0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.Button - UPDATE'), 0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - About'), 0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Privacy Policy'), 0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Terms and Conditions'), 
    0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Help Center'), 0)

Mobile.tap(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Log Out'), 0)

Mobile.verifyElementExist(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Are you sure you want to log out'), 
    0)

Mobile.tap(findTestObject('Check-In App/My Settings Page/android.widget.TextView - NO'), 0)

Mobile.tap(findTestObject('Check-In App/My Settings Page/android.widget.TextView - Log Out'), 0)

Mobile.tap(findTestObject('Check-In App/My Settings Page/android.widget.TextView - YES'), 0)

