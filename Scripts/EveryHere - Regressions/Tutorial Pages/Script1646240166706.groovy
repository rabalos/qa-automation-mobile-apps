import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('EveryHere-Test Cases/Log in page'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(30, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Check-In App/Event List Page/android.widget.TextView - Events'), 0)

Mobile.tap(findTestObject('Check-In App/android.widget.Button -select 1st event'), 0)

Mobile.tap(findTestObject('Check-In App/Event Set-Up/android.widget.Button - START CHECK-IN'), 0)

Mobile.delay(15)

Mobile.tap(findTestObject('Check-In App/Check-in Page/android.widget.ImageButton - Hamburger'), 0)

Mobile.tap(findTestObject('Check-In App/Check In Side Menu/android.widget.TextView - TUTORIAL'), 0)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Quick and Easy Check-In'), 
    0)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Check in event attendees quickly and easily'), 
    0)

Mobile.swipe(400, 300, 200, 300)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Begin Check-In'), 0)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Search for an attendee by scanning the QR code or by name'), 
    0)

Mobile.swipe(400, 300, 200, 300)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Attendee Details'), 0)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - See all the information for a specific attendee'), 
    0)

Mobile.swipe(400, 300, 200, 300)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Attendee ID'), 0)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - To assign an attendee ID number, simply type it in or scan a QR or bar code'), 
    0)

Mobile.swipe(400, 300, 200, 300)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Waiver Signature'), 0)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - Give the device to your attendee so they can read the waiver, sign with their finger, and click Done Signing'), 
    0)

Mobile.swipe(400, 300, 200, 300)

Mobile.verifyElementVisible(findTestObject('Check-In App/Tutorial Pages/android.widget.TextView - All Done'), 0)

Mobile.tap(findTestObject('Check-In App/Tutorial Pages/android.widget.Button - PLAY AGAIN'), 0)

for (int i = 0; i < 5; i++) {
    Mobile.swipe(400, 300, 200, 300)
}

Mobile.tap(findTestObject('Check-In App/Tutorial Pages/android.widget.Button - START CHECK-IN'), 0)

Mobile.swipe(0, 0, 0, 0)

