package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object latestBuild1
     
    /**
     * <p></p>
     */
    public static Object emailOwner
     
    /**
     * <p></p>
     */
    public static Object emailManager
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object eventName
     
    /**
     * <p></p>
     */
    public static Object creditCard
     
    /**
     * <p></p>
     */
    public static Object cvv
     
    /**
     * <p></p>
     */
    public static Object month
     
    /**
     * <p></p>
     */
    public static Object year
     
    /**
     * <p></p>
     */
    public static Object zip
     
    /**
     * <p></p>
     */
    public static Object userFirstName
     
    /**
     * <p></p>
     */
    public static Object userLastName
     
    /**
     * <p></p>
     */
    public static Object latestBuildMac
     
    /**
     * <p></p>
     */
    public static Object latestBuild
     
    /**
     * <p></p>
     */
    public static Object FirstName
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            latestBuild1 = selectedVariables['latestBuild1']
            emailOwner = selectedVariables['emailOwner']
            emailManager = selectedVariables['emailManager']
            password = selectedVariables['password']
            eventName = selectedVariables['eventName']
            creditCard = selectedVariables['creditCard']
            cvv = selectedVariables['cvv']
            month = selectedVariables['month']
            year = selectedVariables['year']
            zip = selectedVariables['zip']
            userFirstName = selectedVariables['userFirstName']
            userLastName = selectedVariables['userLastName']
            latestBuildMac = selectedVariables['latestBuildMac']
            latestBuild = selectedVariables['latestBuild']
            FirstName = selectedVariables['FirstName']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
